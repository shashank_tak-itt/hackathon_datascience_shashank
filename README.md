Requirements :
	import io
	import matplotlib.pyplot as plt             
	%matplotlib inline     
	import pandas as pd
	import numpy as np
	import seaborn as sns                     
	sns.set(color_codes=True)
	from pandas_profiling import ProfileReport
	from sklearn.linear_model import LinearRegression
	from sklearn.preprocessing import LabelEncoder
	from sklearn.model_selection import train_test_split
	from sklearn import metrics
	from sklearn.metrics import r2_score
	
Initial steps to build the solution:
	Reading data from the file, into DataFrames
	Data Exploration and Data Preparation
	Development of Prediction Model
	Checking accuracy
	Use model for prediction

Steps to run:
	Run the ipynb file on your system.
	
